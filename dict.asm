section .text
%include "lib.inc"
global find_word

;rdi=begining of the string, rsi=begining of the dictionary
find_word:
	.loop:
		push rdi ;caller-safed
		push rsi
		add rsi, 8 ;get a string from dict
		call string_equals
		pop rsi
		pop rdi
		test rax, rax
		jnz .succsess
		mov rsi, [rsi]
		test rsi, rsi
		jnz .loop
	.end:
		xor rax, rax
		ret
        .succsess:
                mov rax, rsi
                ret
