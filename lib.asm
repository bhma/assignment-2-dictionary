section .text
global exit
global string_length
global print_string
global print_char
global print_newline
global print_uint
global print_int
global string_equals
global read_char
global read_word
global parse_uint
global parse_int
global string_copy

; Принимает код возврата и завершает текущий процесс
exit: 
    mov rax, 60 ;exit syscall number
    syscall 

; Принимает указатель на нуль-терминированную строку, возвращает её длину
; rdi = beginig of the string, rax will contain the answer
string_length:
    xor rax, rax
    .count:
        cmp byte [rdi+rax], 0
        je .end
        inc rax
        jmp .count
    .end:
        ret

; Принимает указатель на нуль-терминированную строку, выводит её в stdout
; rdi = begining of the string
print_string:
    push rdi ;caller-saved
    call string_length
    pop rdi
    mov rsi, rdi ;begining of the string
    mov rdx, rax ;length
    mov rax, 1 ;syscall number
    mov rdi, 1 ;descriptor
    syscall
    ret 

; Принимает код символа и выводит его в stdout
;rdi = code of the symbol
print_char:
    push rdi
    mov rsi, rsp ; == move rdi to rsi
    mov rax, 1 ;syscall number
    mov rdi, 1 ;descriptor
    mov rdx, 1 ;1 byte only
    syscall
    pop rdi
    ret

; Переводит строку (выводит символ с кодом 0xA)
print_newline:
    mov rdi, 0xA
    jmp print_char

; Выводит беззнаковое 8-байтовое число в десятичном формате 
; Совет: выделите место в стеке и храните там результаты деления
; Не забудьте перевести цифры в их ASCII коды.
;rdi = the number
print_uint:
    mov r8, rsp ;rsp is a callee-saved register and we'll change it
    mov rax, rdi
    dec rsp
    mov byte[rsp], 0 ;the result string will be null-terminated
    mov rsi, 10 ;we'll divide the number by 10 every time to get the decimal number
    .divide:
        xor rdx, rdx
        div rsi
        add rdx, '0' ;convert to ascii
        dec rsp
        mov byte[rsp], dl ;save a symbol in the stack
        test rax, rax ;if register is already null
        jz .print
        jmp .divide
    .print:
        mov rdi, rsp
        push r8 ;caller-saved
        call print_string
        pop r8
        mov rsp, r8 ;callee-saved!
        ret

; Выводит знаковое 8-байтовое число в десятичном формате 
;rdi = the number
print_int:
    test rdi, rdi
    jns print_uint ;if the number is unsigned then use print_uint
    push rdi
    mov rdi, 0x2D ;minus sign
    call print_char
    pop rdi
    neg rdi
    jmp print_uint ;print a minus then print_uint for neg-version

; Принимает два указателя на нуль-терминированные строки, возвращает 1 если они равны, 0 иначе
;rdi = first one, rsi = second one
string_equals:
    .compare:
        mov al, [rdi]
        mov dl, [rsi]
	cmp al, dl
        jne .not_equal ;if current symbols arent equal -> then strings arent equal
        mov al, [rdi]
        test al, al ;if there is a null
        jz .equal
        inc rdi
        inc rsi
        jmp .compare ;go to the next symbol
    .not_equal:
        xor rax, rax
        ret
    .equal:
        mov rax, 1
        ret

; Читает один символ из stdin и возвращает его. Возвращает 0 если достигнут конец потока
read_char:
    dec rsp
    mov byte[rsp], 0
    mov rax, 0 ;syscall number
    mov rdi, 0 ;descriptor
    mov rsi, rsp ;where
    mov rdx, 1 ;length
    syscall
    mov al, [rsp]
    inc rsp ;callee-saved
    ret

; Принимает: адрес начала буфера, размер буфера
; Читает в буфер слово из stdin, пропуская пробельные символы в начале, .
; Пробельные символы это пробел 0x20, табуляция 0x9 и перевод строки 0xA.
; Останавливается и возвращает 0 если слово слишком большое для буфера
; При успехе возвращает адрес буфера в rax, длину слова в rdx.
; При неудаче возвращает 0 в rax
; Эта функция должна дописывать к слову нуль-терминатор
;rdi = begining of the buffer, rsi = size
read_word:
    xor rdx, rdx ;clear rdx for counting
    .read:
        push rdx ;this three registers are caller-saved
        push rdi
        push rsi
        call read_char
        pop rsi ;restore after invoking
        pop rdi
        pop rdx

 ;check size and null-terminate
        cmp rsi, rdx ;check if the word isnt too big
        jz .fail
        mov byte[rdx+rdi], al
        test al, al ;check if its not a null
        jz .end
	cmp al, 0xA
	jz .end
        inc rdx
        jmp .read
    .end:
        mov byte[rdi+rdx], 0
        mov rax, rdi
        ret
    .fail:
        xor rax, rax
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала беззнаковое число.
; Возвращает в rax: число, rdx : его длину в символах
; rdx = 0 если число прочитать не удалось
; rdi = begining of the string
parse_uint:
    xor rdx, rdx
    xor rax, rax
    xor rsi, rsi
    mov r8, 10 ;for mul
    mov r11, rbx ;callee-saved
    .read:
        mov bl, [rdi+rdx]
        sub bl, 0x30
        cmp bl, 0
        js .end
        cmp bl, 0xA
        jns .end
        inc rdx
        push rdx
        mul r8
        pop rdx
        add rax, rbx
        jmp .read
    .end:
        mov rbx, r11 ;callee-saved
        ret

; Принимает указатель на строку, пытается
; прочитать из её начала знаковое число.
; Если есть знак, пробелы между ним и числом не разрешены.
; Возвращает в rax: число, rdx : его длину в символах (включая знак, если он был) 
; rdx = 0 если число прочитать не удалось
parse_int:
    push rdi ;caller-saved
    call read_char
    pop rdi
    cmp al, '-'
    jz .read_neg
    jmp parse_uint
    .read_neg:
        inc rdi
        call parse_uint
        neg rax
        inc rdx
        ret 

; Принимает указатель на строку, указатель на буфер и длину буфера
; Копирует строку в буфер
; Возвращает длину строки если она умещается в буфер, иначе 0
;rdi=begininf of the string, rsi=begining of the buffer, rdx=buffers size
string_copy:
    xor rcx, rcx ;for counting
    push rdi ;caller-saved
    push rsi
    push rdx
    call string_length
    pop rdx
    pop rsi
    pop rdi
    inc rax ;+1 for null-terminate
    cmp rdx, rax
    js .error
    .copy:
        mov r10b, [rdi+rcx]
        mov [rsi+rcx], r10b
        inc rcx
        test r10b, r10b
        jz .end
        jmp .copy
    .error:
        xor rax, rax
        ret
    .end:
        ret
