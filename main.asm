%include "words.inc"
%include "lib.inc"
%include "dict.inc"

section .rodata
	error_string: db "buffer isn't big enough for this word", 0
	no_match_string: db "no match found", 0
	separation: db ": ", 0

section .bss
        buffer: resb 256

section .text

global _start

_start:
	mov rdi, buffer
	mov rsi, 256
	call read_word
	push rdx
	mov rcx, rdx
	mov rdi, rax
	test rax, rax
	jz .error
	mov rsi, pointer
	call find_word
	test rax, rax
	jz .no_match
	add rax, 8
	mov rdi, rax
	push rdi
	call print_string
	mov rdi, separation
	call print_string
	pop rdi
	pop rdx
	add rdi, rdx
	inc rdi
	call print_string
	call print_newline
	mov rdi, 0
	jmp .end
	.error:
		mov rdi, error_string
		call print_error
		call print_newline
		mov rdi, 1
		jmp .end
	.no_match:
		mov rdi, no_match_string
		call print_error
		call print_newline
		mov rdi, 0
	.end:
		call exit

print_error:
	push rdi
	call string_length
	pop rdi
	mov rsi, rdi
	mov rdx, rax
	mov rax, 1
	mov rdi, 2
	syscall
	ret


