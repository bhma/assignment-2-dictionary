.PHONY: clean
ASM=nasm
ASMFLAGS=-felf64 -g
LD=ld

lib.o: lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
dict.o: dict.asm lib.asm
	$(ASM) $(ASMFLAGS) -o $@ $<
main.o: main.asm dict.asm lib.asm colon.inc words.inc
	$(ASM) $(ASMFLAGS) -o $@ $<
program: main.o dict.o lib.o
	$(LD) -o program $+
clean:
	rm *.o
